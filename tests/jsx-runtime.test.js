import { render, Fragment } from "jsx-xml";

describe("jsx-runtime", () => {
	test("single element", () => {
		expect(render(<root />)).toMatchSnapshot();
	});

	test("jsx fragment", () => {
		expect(
			render(
				<Fragment>
					<value>1</value>
					<value>2</value>
					<value>3</value>
				</Fragment>
			)
		).toMatchSnapshot();
	});
});
