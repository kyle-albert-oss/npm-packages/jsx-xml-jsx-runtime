const { JSXXML } = require("jsx-xml");

const jsxToXml = (type, props) => {
	const attr = {
		...(props || undefined),
	};
	delete attr.children;

	let children = props ? props.children : undefined;
	if (!Array.isArray(children)) {
		children = [children];
	}

	return JSXXML(type, attr, ...children);
};

module.exports = {
	jsxToXml,
};
