const { jsxToXml } = require("./shared");

module.exports = {
	jsx: jsxToXml,
	jsxs: jsxToXml,
};
