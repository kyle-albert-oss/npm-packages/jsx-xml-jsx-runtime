# jsx-xml-jsx-runtime

A JSX runtime adapter for [jsx-xml](https://www.npmjs.com/package/jsx-xml).

---

## Installation

1. Install [jsx-xml](https://www.npmjs.com/package/jsx-xml) and this package.
   ```shell
   npm i jsx-xml jsx-xml-jsx-runtime
   ```
2. If you are using [create-react-app](https://create-react-app.dev) you're done with installation.
3. Install Babel plugin [transform-react-jsx](https://babeljs.io/docs/en/babel-plugin-transform-react-jsx).
4. Ensure the configuration options for the plugin look something like this:
   ```json
   {
     "plugins": [
       [
         "@babel/plugin-transform-react-jsx",
         {
           "throwIfNamespace": false, // false to allow xml namespaces
           "runtime": "automatic", // defaults to classic
           // only use the following if globally using jsx based xml, otherwise use JSDoc approach per file (see usage section)
           "importSource": "jsx-xml-jsx-runtime" // defaults to react
         }
       ]
     ]
   }
   ```

## Usage

Refer to the [transform-react-jsx Babel plugin documentation](https://babeljs.io/docs/en/babel-plugin-transform-react-jsx) for usage instructions.

### Usage with React/create-react-app

Assuming you are using create-react-app or have configured the transform-react-jsx to automatically use React as the JSX runtime, you will need to
put your JSX based XML in a separate JS file and use a JSDoc comment to specify this library as the JSX runtime for that file. See example below and [here](https://babeljs.io/docs/en/babel-plugin-transform-react-jsx#customizing-the-automatic-runtime-import) for details.

Example:

#### MyReactComponent.js

```javascript
import { render } from "jsx-xml";
import xml from "./XmlData.js";

export const MyReactComponent = (props) => {
  return <div>{render(xml)}</div>;
  // renders as xml string
};
```

#### XmlData.js

```javascript
import { Fragment } from "jsx-xml";

/**
 * @jsxImportSource jsx-xml-jsx-runtime
 */

const xml = (
  <thiz>
    <is />
    <Fragment>
      <cool howMuch="really" num={5} />
    </Fragment>
  </thiz>
);

// note: using "this" as the jsx tag results in error.

export default xml;
```
